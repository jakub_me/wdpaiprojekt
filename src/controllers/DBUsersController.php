<?php

require_once 'SessionControllers.php';
require_once __DIR__ . "/../models/UserFull.php";
require_once __DIR__ . "/../models/UserUpdate.php";
require_once __DIR__."/../repository/UserRepository.php";

class DBUsersController extends SessionControllers
{
    private $userRepository;
    private $rights;
    public function __construct()
    {
        parent::__construct();
        $this->userRepository = new UserRepository();
        $this->rights = $this->getPrivilegesRepository()->getRightsList();
    }

    //display function
    public function baseusers()
    {
        if(!$this->checkPrivileges(['dbadmin']))
            return $this->render('login');
        $users = $this->userRepository->getDBUsers();
        $userTypes = $this->getPrivilegesRepository()->getUserRightsAllUsers();
        $this->render('baseusers',[
            'rights' => $this->rights,
            'messages' => $this->messages,
            'users' => $users,
            'types' => $userTypes
        ]);
    }

    public function modifyUser()
    {
        if($this->isGet())
        {
            $url = "http://$_SERVER[HTTP_HOST]";
            header("Location: {$url}/panel");
            return;
        }
        if(!$this->checkPrivileges(['dbadmin']))
            return $this->render('login');

        $id = $_POST['id'];
        $detid = $_POST['id_user_details'];
        $email = $_POST['text'];
        $rightsArr = $_POST['rights'];
        if(isset($_POST['rem']))
        {
            $this->userRepository->deleteUser($id, $detid);
            $this->killSessionsOfUser($id);
            return $this->baseusers();
        }
        elseif(isset($_POST['edit']))
        {
            if($this->checkLength($email,1,100))
                return $this->baseusers();

            if(isset($_POST['login_enabled']))
                $enabled = true;
            else
            {
                $enabled = false;
                $this->killSessionsOfUser($id);
            }
            $this->userRepository->adminModifyUser(new UserUpdate($id, $email, $enabled, $rightsArr));
            return $this->baseusers();
        }
        die();
    }



    public function searchUsersByRights()
    {
        if($this->isGet())
        {
            $url = "http://$_SERVER[HTTP_HOST]";
            header("Location: {$url}/panel");
            return;
        }
    }


}