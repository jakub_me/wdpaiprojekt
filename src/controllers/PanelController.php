<?php

require_once "SessionControllers.php";
require_once __DIR__ . '/../templates/panelButtons.php';

class PanelController extends SessionControllers
{
    private $userRepository;
    public function __construct()
    {
        parent::__construct();
        $this->userRepository = new UserRepository();
    }

    public function panel()
    {
        $rightsArray = ['koszyk','dbadmin','panel-menu'];
        $rights = $this->getPrivileges($rightsArray);
        if(empty($rights))
            return $this->render('login');
        $outputButtons = $this->prepareButtons($rights);
        return $this->render('panel', [
            'img' => 1,
            'h2' => 0,
            'title' => 'Panel konta',
            'buttons' => $outputButtons,
            'messages' => $this->messages
        ]);
    }

    private function prepareButtons($rights)
    {
        $outputButtons = [];
        $outputButtons[] = createLI('passchange','ZMIANA HASŁA');
        if(in_array('koszyk',$rights))
            $outputButtons[] = createLI('koszyk','KOSZYK');
        if(in_array('dbadmin',$rights))
            $outputButtons[] = createLI('dbpanel','BAZA DANYCH');
        if(in_array('panel-menu',$rights))
            $outputButtons[] = createLI('menu','MENU');

        $outputButtons[] = createLI('/','STRONA GŁÓWNA');
        $outputButtons[] = createLI('logout','WYLOGUJ');
        return $outputButtons;
    }
}