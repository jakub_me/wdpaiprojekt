<?php

require_once "SessionControllers.php";
require_once __DIR__ . '/../templates/panelButtons.php';

class HomepageController extends SessionControllers
{
    public function __construct()
    {
        parent::__construct();
    }

    public function main1()
    {
        $rightsArray = (['/','koszyk']);
        $rights = $this->getPrivileges($rightsArray);
        $outputButtons = $this->prepareButtons($rights);
        return $this->render('panel', [
            'img' => 0,
            'h2' => 0,
            'title' => 'Strona główna',
            'buttons' => $outputButtons,
            'messages' => $this->messages
        ]);
    }

    public function logout()
    {
        $this->endSession();
        return $this->main1();
    }

    private function prepareButtons($rights)
    {
        $outputButtons = [];
        $outputButtons[] = createLI('kontakt','!!!!!KONTAKT');
        $outputButtons[] = createLI('menu','MENU');
        if(empty($rights))
            $outputButtons[] = createLI('login','LOGOWANIE');
        else
        {
            $outputButtons[] = createLI('panel','PANEL UŻYTKOWNIKA');
            $outputButtons[] = createLI('logout','WYLOGUJ');
        }
        return $outputButtons;
    }
}