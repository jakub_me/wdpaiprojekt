<?php

require_once "SessionControllers.php";
require_once __DIR__ . "/../predefinedSettings/passSettings.php";
require_once __DIR__ . '/../templates/panelButtons.php';
require_once __DIR__."/../predefinedSettings/DBRecords.php";

class DBController extends SessionControllers
{
    private $userRepository;
    public function __construct()
    {
        parent::__construct();
        $this->userRepository = new UserRepository();
    }

    public function dbpanel()
    {
        $rightsArray = ['dbadmin'];
        $rights = $this->getPrivileges($rightsArray);
        if(empty($rights))
            return $this->render('login');
        $outputButtons = $this->prepareButtons($rights);
        return $this->render('panel', [
            'img' => 1,
            'h2' => 0,
            'title' => 'Baza danych',
            'buttons' => $outputButtons,
            'messages' => $this->messages
        ]);
    }

    private function prepareButtons($rights)
    {
        $outputButtons = [];
        $outputButtons[] = createLI('baseusers','WIDOK UŻYTKOWNICY');
        $outputButtons[] = createLI('basejednostki','TABELA JEDNOSTKI');
        $outputButtons[] = createLI('basejedzenie','TABELA JEDZENIE');
        $outputButtons[] = createLI('basemenu','WIDOK MENU');
        $outputButtons[] = createLI('panel','POWRÓT');
        return $outputButtons;
    }

}