<?php

require_once 'AppController.php';
require_once __DIR__ . "/../models/JednostkaMiary.php";
require_once __DIR__."/../repository/JednostkaRepository.php";

class DBJednostkaController extends SessionControllers
{
    private $repo;
    public function __construct()
    {
        parent::__construct();
        $this->repo = new JednostkaRepository();
    }

    //display
    public function basejednostki()
    {
        if(!$this->checkPrivileges(['dbadmin']))
            return $this->render('login');
        $items = $this->repo->getJednostki();
        return $this->render('basejednostki',[
            'items' => $items,
            'messages' => $this->messages
        ]);
    }

    public function addJednostka()
    {
        $url = "http://$_SERVER[HTTP_HOST]";
        if($this->isGet())
        {
            header("Location: {$url}/panel");
            return $this->render('login');
        }

        if(!$this->checkPrivileges(['dbadmin']))
            return $this->render('login');

        $nazwa = $_POST['text'];
        if($this->checkLength($nazwa,1,10))
            return $this->basejednostki();

        $jednostka = $this->repo->getJednostka($nazwa);
        if($jednostka !== null)
        {
            $this->messages[] = 'Jednostka o podanej nazwie juz istnieje';
            return $this->basejednostki();
        }
        $this->repo->addJednostka($nazwa);
        return $this->basejednostki();
    }

    public function modifyJednostka()
    {
        if($this->isGet())
        {
            $url = "http://$_SERVER[HTTP_HOST]";
            header("Location: {$url}/login");
            return $this->render('login');
        }
        if(!$this->checkPrivileges(['dbadmin']))
            return $this->render('login');

        if(isset($_POST['rem']))
        {
            $this->repo->deleteJednostka($_POST['id']);
            return $this->basejednostki();
        }
        elseif(isset($_POST['edit']))
        {
            if($this->checkLength($_POST['text'],1,10))
                return $this->basejednostki();
            $this->repo->modifyJednostka(new JednostkaMiary($_POST['id'],$_POST['text']));
            return $this->basejednostki();
        }
        die();
    }
}