<?php

require_once 'AppController.php';
require_once __DIR__."/../repository/SessionRepository.php";
require_once __DIR__."/../repository/PrivilegesRepository.php";
require_once __DIR__."/../predefinedSettings/Privileges.php";

class SessionControllers extends AppController
{
    private SessionRepository $sessionRepository;
    private PrivilegesRepository $privilegesRepository;

    protected function getPrivilegesRepository(): PrivilegesRepository
    {
        return $this->privilegesRepository;
    }

    protected function __construct()
    {
        parent::__construct();
        $this->privilegesRepository = new PrivilegesRepository();
        if(isset($_COOKIE['session']))
            $this->sessionRepository = new SessionRepository($_COOKIE['session']);
    }



    protected function startSession(int $userID):void
    {
        $cookieID = hash('sha256',strval($userID).HASHALL);
        setcookie("session", $cookieID, time() + 3600, "/");
        $this->sessionRepository = new SessionRepository($cookieID);
        $this->sessionRepository->login($userID);
    }

    protected function endSession()
    {
        if(isset($_COOKIE['session']))
            $this->sessionRepository->logout();
        setcookie("session","",time() - 555, "/");
    }

    protected function killSessionsOfUser(int $id): void
    {
        $this->sessionRepository->killSessionByUserID($id);
    }


    protected function checkSession(): bool
    {
        if(isset($_COOKIE['session']))
            return $this->sessionRepository->checkCookie($_COOKIE['session']);
        return false;
    }

    protected function getUserIdFromSession(): ?int
    {
        if(isset($_COOKIE['session']))
            return $this->sessionRepository->getUserId($_COOKIE['session']);
        return null;
    }




    protected function checkPrivileges(array $arr): bool
    {
        $b = $this->getPrivileges($arr);
            return ( count( $arr ) == count( $b ) && !array_diff( $arr, $b ) );
    }

    protected function getPrivileges(array $arr): array
    {
        $userID = $this->getUserIdFromSession();
        if(empty($userID))
            return [];
        $accTypes = $this->privilegesRepository->getUserRightsById($userID);
        if($accTypes == null)
            return []; //user without rights
        $result = [];
        foreach ($arr as $key) {
            if(Privileges::checkRights($accTypes, $key))
                $result[] = $key;
        }
        return $result;
    }


}