<?php

require_once "SessionControllers.php";
require_once __DIR__ . "/../models/UserShort.php";
require_once __DIR__ . "/../models/UserRegistering.php";
require_once __DIR__ . "/../models/UserInfo.php";
require_once __DIR__ . "/../models/UserPhone.php";
require_once __DIR__."/../repository/UserRepository.php";

require_once __DIR__ . "/../predefinedSettings/passSettings.php";
require_once __DIR__ . '/../predefinedSettings/defaultRights.php';

class SecurityController extends SessionControllers
{
    private $userRepository;
    public function __construct()
    {
        parent::__construct();
        $this->userRepository = new UserRepository();
    }


    public function login()
    {
        if($this->checkSession())
        {
            $url = "http://$_SERVER[HTTP_HOST]";
            header("Location: {$url}/panel");
            return;
        }
        if($this->isGet())
            return $this->render('login');

        $email = $_POST['email'];
        $password = $_POST['password'];

        if($this->checkLength($email,1,100, 'Podano pusty login')
            || $this->checkLength($password,1,500,'Nie podano hasla'))
            return $this->render('login',['messages' => $this->messages]);

        $user = $this->userRepository->getUser($email);
        if (!$user)
            return $this->render('login', ['messages' => ['User not exist!']]);
        if ($user->getName() !== $email)
            return $this->render('login', ['messages' => ['User with this email not exist!']]);
        if ($user->isLoginEnabled() == false)
            return $this->render('login', ['messages' => ["This user is disabled."]]);

        if ($user->getPassword() !== hash('sha256', $password . $user->getSalt() . HASHALL))
            return $this->render('login', ['messages' => ['Wrong password!']]);

        $this->startSession($user->getId());

        $url = "http://$_SERVER[HTTP_HOST]";
        header("Location: {$url}/panel");
        return;
    }





    public function register()
    {
        if($this->checkSession())
        {
            $url = "http://$_SERVER[HTTP_HOST]";
            header("Location: {$url}/panel");
            return;
        }
        $this->render('register');
    }

    public function newUser()
    {
        if($this->isGet())
        {
            $url = "http://$_SERVER[HTTP_HOST]";
            header("Location: {$url}/register");
            return;
        }
        if($this->checkSession())
        {
            $url = "http://$_SERVER[HTTP_HOST]";
            header("Location: {$url}/panel");
            return;
        }

        $email = $_POST['email'];
        $password = $_POST['password'];
        $confirmPassword = $_POST['confirmPassword'];
        $firstname = $_POST['firstname'];
        $surname = $_POST['surname'];
        $phone = $_POST['phone'];
        if($password != $confirmPassword)
            return $this->render('register',['messages' => ['Hasla sie nie pokrywaja']]);
        if($this->checkLength($email,1,100)
            || $this->checkLength($password,8,500, 'Haslo musi miec minimum 8 znaków')
            || $this->checkLength($firstname,1,50, 'Nie podano imienia')
            || $this->checkLength($surname,1,50, 'Nie podano nazwisko')
            || $this->checkLength($phone,0,15))
            return $this->render('register',['messages' => $this->messages]);

        if(empty($phone))
            $phone = null;
        $user = $this->userRepository->getUser($email);

        if($user)
            return $this->render('register',['messages' => ['This login is already in use!']]);
        $userSalt = $this->generateSalt();
        $hashed = hash('sha256',$password.$userSalt.HASHALL);
        $newUser = new UserRegistering($email, $hashed, $userSalt, $firstname, $surname, $phone);
        $user = $this->userRepository->addUser($newUser, DEFAULTRIGHTS); //returns int id

        $this->startSession($user);
        $url = "http://$_SERVER[HTTP_HOST]";
        header("Location: {$url}/panel");
        return;
    }




    public function passchange()
    {
        $userID = $this->getUserIdFromSession();
        if($userID == null)
            return $this->render('login');
        $user= $this->userRepository->getUserInfo($userID);
        return $this->render('passchange', [
            'user' => $user,
            "messages" => $this->messages
        ]);
    }

    public function changePass()
    {
        $userID = $this->getUserIdFromSession();
        if($userID == null)
            return $this->render('login');
        $oldPass = $_POST['oldpass'];
        $newPass = $_POST['newpass'];
        $confirmPass = $_POST['newpassConfirmed'];
        if($newPass != $confirmPass)
        {
            $this->messages[] = 'Hasla sie nie pokrywaja';
            return $this->passchange();
        }
        if($oldPass == $newPass)
        {
            $this->messages[] = 'Stare haslo takie samo jak nowe?';
            return $this->passchange();
        }
        if($this->checkLength($newPass,8,500, 'Haslo musi miec minimum 8 znaków'))
            return $this->passchange();
        $user = $this->userRepository->getUserById($userID);
        if($user->getPassword() !== hash('sha256',$oldPass.$user->getSalt().HASHALL))
        {
            $this->messages[] = 'Wrong password';
            return $this->passchange();
        }
        $user->setSalt($this->generateSalt());
        $user->setPassword(hash('sha256',$newPass.$user->getSalt().HASHALL));
        $this->userRepository->setPassword($user);
        $this->messages[] = 'Hasło zmieniono pomyślnie :)';
        return $this->passchange();
    }

    public function changePhone()
    {
        $userID = $this->getUserIdFromSession();
        if($userID == null)
            return $this->render('login');
        $phone = $_POST['phone'] ?: null;
        if($this->checkLength($phone,0,15))
            return $this->render('passchange',['messages' => $this->messages]);
        $this->userRepository->setPhone(new UserPhone($userID, $phone));
        $this->messages[] = 'Nr telefonu zmieniono pomyślnie :)';
        return $this->passchange();
    }



    private function generateSalt($length = 40)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++)
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        return $randomString;
    }
}