<?php

require_once 'SessionControllers.php';
require_once __DIR__ . "/../models/JedzenieFull.php";
require_once __DIR__ . "/../models/MenuItem.php";
require_once __DIR__ . '/../models/Danie.php';
require_once __DIR__ . '/../models/DanieFull.php';
require_once __DIR__ . '/../models/Skladnik.php';
require_once __DIR__ . '/../models/SkladnikFull.php';
require_once __DIR__."/../repository/MenuRepository.php";

class DBMenuController extends SessionControllers
{
    private $menuRepository;
    private $jedzenie;
    public function __construct()
    {
        parent::__construct();
        $this->menuRepository = new MenuRepository();
        $this->jedzenie = $this->menuRepository->getJedzenie();
    }

    public function basemenu()
    {
        if(!$this->checkPrivileges(['dbadmin']))
            return $this->render('login');
        $dania = $this->menuRepository->getDania(false);
        return $this->render('basemenu',[
            'dania' => $dania,
            'jedzenia' => $this->jedzenie,
            'messages' => $this->messages
        ]);
    }

    public function deleteDanie()
    {
        if(!$this->checkPrivileges(['dbadmin']))
            return $this->render('login');
        $id = (int)$_POST['danieId'];
        $this->menuRepository->deleteDanie($id);
        return $this->basemenu();
    }

    public function selectDanie()
    {
        $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
        if ($contentType === "application/json")
        {
            $content = trim(file_get_contents("php://input"));
            $decoded = json_decode($content, true);

            header('Content-type: application/json');
            http_response_code(200);
            $x = $this->menuRepository->getSkladniki($decoded['modify']);
            echo json_encode($x);
        }
    }

    public function manageMenu()
    {
        if($this->checkLength($_POST['text'],1,100,"brak nazwy")
        || $this->checkLength($_POST['cena'],1,5,"wymagana cena"))
            return $this->basemenu();
        if(0 >= (int) $_POST['cena'])
        {
            $this->messages[] = "Wpisz niezerowa kwote";
            return $this->basemenu();
        }
        if(!$this->checkPrivileges(['dbadmin']))
            return $this->render('login');
        return isset($_POST['new']) ? $this->addDanie() : $this->modifyDanie();
    }

    private function addDanie()
    {
        $danie = new Danie($_POST['text'],(int) $_POST['cena']);
        $danieFull = $this->menuRepository->addDanie($danie);
        $this->addSkladniki($danieFull);
        return $this->basemenu();
    }

    private function modifyDanie()
    {
        return $this->basemenu();
    }

    private function addSkladniki(DanieFull $danie)
    {
        $skladniki = [];
        $jedzenieIDs = $_POST['ids'];
        foreach ($jedzenieIDs as $item) {
            $ilosc = (int) $_POST["n$item"];
            $skladniki[] = new Skladnik((int)$item, $danie->getId(), $ilosc);
        }
        $this->menuRepository->setSkladniki($skladniki);
    }
}