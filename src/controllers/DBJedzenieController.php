<?php

require_once 'AppController.php';
require_once __DIR__ . "/../models/Jedzenie.php";
require_once __DIR__."/../repository/JednostkaRepository.php";
require_once __DIR__."/../repository/JedzenieRepository.php";

class DBJedzenieController extends SessionControllers
{
    private $jedzenieRepository;
    public function __construct()
    {
        parent::__construct();
        $this->jedzenieRepository = new JedzenieRepository();
    }

    //display
    public function basejedzenie()
    {
        if(!$this->checkPrivileges(['dbadmin']))
            return $this->render('login');
        $jednostkiRepo = new JednostkaRepository();
        $jednostki = $jednostkiRepo->getJednostki();
        $jedzenie = $this->jedzenieRepository->getJedzenie();
        $this->render('basejedzenie',[
            'jednostki' => $jednostki,
            'jedzenia' => $jedzenie,
            'messages' => $this->messages
        ]);
    }

    public function addJedzenie()
    {
        if($this->isGet())
        {
            $url = "http://$_SERVER[HTTP_HOST]";
            header("Location: {$url}/panel");
            return;
        }
        if(!$this->checkPrivileges(['dbadmin']))
            return $this->render('login');
        $name = $_POST['text'];
        $jedn = (int) $_POST['jednostka'];

        if($this->checkLength($name,1,50))
            return $this->basejedzenie();

        $this->jedzenieRepository->addJedzenie($name, $jedn);
        return $this->basejedzenie();
    }

    public function modifyJedzenie()
    {
        if($this->isGet())
        {
            $url = "http://$_SERVER[HTTP_HOST]";
            header("Location: {$url}/panel");
            return;
        }
        if(!$this->checkPrivileges(['dbadmin']))
            return $this->render('login');
        //header("Location: {$url}/displayDBJednostki");
        $id = $_POST['id'];
        $name = $_POST['text'];
        $jedn = (int) $_POST['jednostka'];
        if(isset($_POST['rem']))
        {
            $this->jedzenieRepository->deleteJedzenie($id);
            return $this->basejedzenie();
        }
        elseif(isset($_POST['edit']))
        {
            if($this->checkLength($name,1,50))
                return $this->basejedzenie();
            $this->jedzenieRepository->modifyJedzenie(new Jedzenie($id, $name, $jedn));
            return $this->basejedzenie();
        }
        die();
    }
}