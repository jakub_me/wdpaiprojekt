<?php

require_once "SessionControllers.php";
require_once __DIR__ . "/../models/MenuItem.php";
require_once __DIR__."/../repository/MenuRepository.php";

class KoszykController extends SessionControllers
{
    const MAX_FILE_SIZE = 1024*1024;
    const SUPPORTED_TYPES = ['image/png', 'image/jpeg'];
    const UPLOAD_DIR = '/../public/uploads/';
    private $addr;

    public function __construct()
    {
        parent::__construct();
    }

    public function koszyk()
    {
        if(!$this->checkPrivileges(['koszyk']))
            return $this->render('login');
        $cookies = json_decode($_COOKIE['koszyk'],true);
        if(empty($cookies))
        {
            $this->messages[] = 'Twój koszyk jest pusty.';
            return $this->render('koszyk',[
                'sell' => false,
                'cena' => 0,
                'koszyk' => [],
                'count' => [],
                'messages' => $this->messages
            ]);
        }
        $this->generateKoszyk($cookies);
    }

    private function generateKoszyk($cookies)
    {
        $koszykList = [];
        $countList = [];
        $cena = 0;
        $menuRepository = new MenuRepository();
        $menuList = $menuRepository->getDania(true);
        foreach ($menuList as $danie) {
            $id = $danie->getId();
            if(isset($cookies["$id"]))
            {
                $koszykList[] = $danie;
                $countList[$id] = $cookies["$id"];
                $cena += $danie->getCena() * (int) $cookies[$id];
            }
        }
        $this->render('koszyk',[
            'sell' => true,
            'cena' => $cena,
            'koszyk' => $koszykList,
            'count' => $countList,
            'messages' => $this->messages
        ]);
    }

    public function koszykBuy()
    {
        if(!$this->checkPrivileges(['koszyk']))
            return $this->render('login');
        if(isset($_POST['accept']))
        {
            setcookie("koszyk", "", time() - 360000, "/");
            $this->addr = $_POST['address'];
            if(is_uploaded_file($_FILES['file']['tmp_name']))
            {
                $this->manageFile();
                $this->generateKoszyk(null);
            }
            $this->messages[] = 'Zatwierdzone do realizacji :)';
            $this->generateKoszyk(null);
        }

    }

    private function manageFile(): bool
    {
        if($this->validateFile($_FILES['file']))
        {
            move_uploaded_file(
                $_FILES['file']['tmp_name'],
                dirname(__DIR__).self::UPLOAD_DIR.$_FILES['file']['name']
            );
            $this->messages[] = 'Zatwierdzone do realizacji z fotką :)';
            return true;
        }
        return false;
    }

    private function validateFile(array $file): bool
    {
        if($file['size'] > self::MAX_FILE_SIZE){
            $this->messages[] = 'Plik jest zbyt duzy. Nie przyjalem zamowienia.';
            return false;
        }
        if(!in_array($file['type'], self::SUPPORTED_TYPES)){
            $this->messages[] = 'Nieobslugiwany format pliku. Nie przyjalem zamowienia.';
            return false;
        }
        return true;
    }
}