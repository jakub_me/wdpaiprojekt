<?php

require_once 'AppController.php';

class MenuController extends AppController
{
    //display
    public function menu()
    {
        $menuRepo = new MenuRepository();
        $dania = $menuRepo->getDania(true);
        $this->render('menu', ['dania' => $dania, 'messages' => $this->messages]);
    }
}