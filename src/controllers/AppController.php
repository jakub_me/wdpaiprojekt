<?php

class AppController{

    private $request;
    protected array $messages = [];

    public function __construct()
    {
        $this->request = $_SERVER['REQUEST_METHOD'];
    }

    protected function render(string $template = null, array $variables = []) {
        $templatePath = 'public/views/'.$template.'.php';
        $output = "File not found.";
        if(file_exists($templatePath)){
            extract($variables);

            //wyswietlanie strony
            ob_start();
            include  $templatePath;
            $output = ob_get_clean();
        }
        print $output;
    }

    protected function isGet(): bool{
        return $this->request === 'GET';
    }
    protected function isPost(): bool{
        return $this->request === 'POST';
    }

    protected function checkLength($str, int $min=1, int $max=10, ?string $msg1=null, ?string $msg2=null): bool
    {
        if(strlen($str) > $max)
        {
            $this->messages[] = $msg2 ?? 'Przekroczona dlugosc stringa.';
            return true;
        }
        if(strlen($str) < $min)
        {
            $this->messages[] = $msg1 ?? 'Podano zbyt krótki napis';
            return true;
        }
        return false;
    }


}