<?php

require_once 'Repository.php';
require_once __DIR__ . '/../models/JedzenieFull.php';
require_once __DIR__ . '/../models/MenuItem.php';
require_once __DIR__ . '/../models/Danie.php';
require_once __DIR__ . '/../models/DanieFull.php';
require_once __DIR__ . '/../models/Skladnik.php';
require_once __DIR__ . '/../models/SkladnikFull.php';
class MenuRepository extends Repository
{
    public function __construct()
    {
        parent::getInstance();
    }

    public function getDania(bool $enabledRequire = false): array
    {
        $items =  $enabledRequire ? $this->getDaniaEnabled() :$this->getDaniaAll();
        if(empty($items))
            return [];
        $result = [];

        foreach ($items as $danie){
            $result[] = new MenuItem(
                $danie['id'],
                $danie['nazwa'],
                $danie['cena'],
                $danie['arr'],
                true
            );
        }
        return $result;
    }
    private function getDaniaEnabled(): ?array
    {
        $stat = parent::getInstance()->connect()->prepare('
            select * from vmenu_display where enabled = true
        ');
        $stat->execute();
        return $stat->fetchAll(PDO::FETCH_ASSOC);
    }

    private function getDaniaAll(): ?array
    {
        $stat = parent::getInstance()->connect()->prepare('
            select * from vmenu_display
        ');
        $stat->execute();
        return $stat->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getJedzenie(): ?array
    {
        $result = [];
        $stat = parent::getInstance()->connect()->prepare('
            select * from vjedzenie
        ');
        $stat->execute();
        $items = $stat->fetchAll(PDO::FETCH_ASSOC);
        if(empty($items))
            return null;
        foreach ($items as $danie){
            $result[] = new JedzenieFull(
                $danie['id'],
                $danie['nazwa'],
                $danie['jednostka']
            );
        }
        return $result;
    }

    public function getSkladniki(int $id)
    {
        $stat = parent::getInstance()->connect()->prepare('
            select v.id id, v.nazwa nazwa, v.jednostka jednostka, m.ilosc ilosc
            from vjedzenie v 
            JOIN menu_jedzenie m ON m.id_jedzenie = v.id 
            WHERE m.id_menu = :id
        ');
        $stat->bindParam(':id',$id,PDO::PARAM_INT);
        $stat->execute();
        return $stat->fetchAll(PDO::FETCH_ASSOC);
    }

    public function addDanie(Danie $danie): DanieFull
    {
        $stat = parent::getInstance()->connect()->prepare('
            INSERT INTO menu (nazwa_potrawy, cena)
            VALUES (:nazwa, :cena)
        ');
        $nazwa = $danie->getNazwa();
        $cena = $danie->getCena();
        $stat->bindParam(':nazwa',$nazwa,PDO::PARAM_STR);
        $stat->bindParam(':cena',$cena,PDO::PARAM_INT);
        $stat->execute();
        return $this->getDanieFull($danie);
    }

    public function getDanieFull(Danie $danie)
    {
        $cena = $danie->getCena();
        $nazwa = $danie->getNazwa();
        $stat = parent::getInstance()->connect()->prepare('
            SELECT id, is_enabled FROM menu WHERE nazwa_potrawy = :nazwa AND cena= :cena
        ');
        $stat->bindParam(':nazwa',$nazwa,PDO::PARAM_STR);
        $stat->bindParam(':cena',$cena,PDO::PARAM_INT);
        $stat->execute();
        $item = $stat->fetch(PDO::FETCH_ASSOC);
        return new DanieFull(
            $danie->getNazwa(),
            $danie->getCena(),
            $item['id'],
            $item['is_enabled'],
        );
    }

    public function setSkladniki(array $skladniki): void
    {
        $stat = parent::getInstance()->connect()->prepare('
            INSERT INTO menu_jedzenie (id_menu, id_jedzenie, ilosc)
            VALUES (:menu, :jedz, :ilosc)
        ');
        foreach ($skladniki as $item) {
            $idMenu = $item->getIdMenu();
            $idJedz = $item->getIdJedz();
            $ilosc = $item->getIlosc();
            $stat->bindParam(':menu',$idMenu,PDO::PARAM_INT);
            $stat->bindParam(':jedz',$idJedz,PDO::PARAM_INT);
            $stat->bindParam(':ilosc',$ilosc,PDO::PARAM_INT);
            $stat->execute();
        }
    }

    public function deleteDanie(int $id): void
    {
        $stat = parent::getInstance()->connect()->prepare('
            DELETE FROM menu WHERE id = :id
        ');
        $stat->bindParam(':id',$id,PDO::PARAM_INT);
        $stat->execute();
    }

}