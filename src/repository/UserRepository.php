<?php

require_once 'Repository.php';
require_once __DIR__ . '/../models/UserShort.php';
require_once __DIR__ . '/../models/UserRegistering.php';
require_once __DIR__ . "/../models/UserInfo.php";
require_once __DIR__ . '/../models/UserFull.php';
require_once __DIR__ . "/../models/UserUpdate.php";
require_once __DIR__ . "/../models/UserPhone.php";
require_once __DIR__ . '/../models/Rights.php';

class UserRepository extends Repository
{
    public function __construct()
    {
        parent::getInstance();
    }



    public function getUser(string $email): ?UserShort
    {
        $stat = parent::getInstance()->connect()->prepare('SELECT * FROM users WHERE email = :email');

        $stat->bindParam(':email',$email,PDO::PARAM_STR);
        $stat->execute();

        $user = $stat->fetch(PDO::FETCH_ASSOC);
        if($user == false)
            return null;

        return new UserShort(
            $user['email'],
            $user['password'],
            $user['salt'],
            filter_var($user['login_enabled'], FILTER_VALIDATE_BOOLEAN),
            $user['id']
        );
    }
    public function getUserById(int $id): ?UserShort
    {
        $stat = parent::getInstance()->connect()->prepare('SELECT * FROM users WHERE id = :id');

        $stat->bindParam(':id',$id,PDO::PARAM_INT);
        $stat->execute();

        $user = $stat->fetch(PDO::FETCH_ASSOC);
        if($user == false)
            return null;

        return new UserShort(
            $user['email'],
            $user['password'],
            $user['salt'],
            filter_var($user['login_enabled'], FILTER_VALIDATE_BOOLEAN),
            $user['id']
        );
    }

    public function addUser(UserRegistering $user, ?array $rights): int
    {
        $stat = parent::getInstance()->connect()->prepare('
            INSERT INTO user_details (name, surname, phone)
            VALUES (?, ?, ?)
        ');
        $stat->execute([
            $user->getFirstname(),
            $user->getSurname(),
            $user->getPhone()
        ]);

        $stat = parent::getInstance()->connect()->prepare('
            INSERT INTO users (email, password, salt, id_user_details)
            VALUES (?, ?, ?, ?)
        ');
        $stat->execute([
            $user->getUsername(),
            $user->getPassword(),
            $user->getSalt(),
            $this->getUserDetailsId($user),
        ]);

        $userID = $this->getId($user->getUsername());

        $stat = parent::getInstance()->connect()->prepare('
                INSERT INTO users_types (id_users, id_account_type)
                VALUES (?, ?)
            ');
        foreach ($rights as $right){
            $stat->execute([
                $userID,
                $this->getRightId($right)
            ]);
        }
        return $userID;
    }

    public function getId(string $email): ?int
    {
        $stat = parent::getInstance()->connect()->prepare('SELECT id FROM users WHERE email = :email');
        $stat->bindParam(':email',$email,PDO::PARAM_STR);
        $stat->execute();

        $user = $stat->fetch(PDO::FETCH_ASSOC);
        if($user == false)
            return null;
        return $user['id'];
    }





    public function getUserInfo(int $id): UserInfo
    {
        $stat = parent::getInstance()->connect()->prepare('
        SELECT  d.name as name, d.surname as surname, d.phone as phone
        FROM users u
        JOIN user_details as d ON u.id_user_details = d.id  
        WHERE u.id = :id');
        $stat->bindParam(':id',$id,PDO::PARAM_INT);
        $stat->execute();

        $user = $stat->fetch(PDO::FETCH_ASSOC);
        return new UserInfo(
            $user['name'],
            $user['surname'],
            $user['phone']
        );
    }

    public function setPassword(UserShort $user): void
    {
        $email = $user->getName();
        $pass = $user->getPassword();
        $salt = $user->getSalt();
        $stat = parent::getInstance()->connect()->prepare('
            UPDATE users SET salt=:salt, password=:pass WHERE email = :email
        ');
        $stat->bindParam(':salt',$salt,PDO::PARAM_STR);
        $stat->bindParam(':pass',$pass,PDO::PARAM_STR);
        $stat->bindParam(':email',$email,PDO::PARAM_STR);
        $stat->execute();
    }

    public function setPhone(UserPhone $user): void
    {
        // join not available in UPDATE ;(
        $id = $user->getId();
        $phone = $user->getPhone();
        $stat = parent::getInstance()->connect()->prepare('
            UPDATE user_details d SET phone=:phone 
            FROM users u
            WHERE d.id = u.id_user_details
            AND u.id = :id
        ');
        $stat->bindParam(':id',$id,PDO::PARAM_INT);
        $stat->bindParam(':phone',$phone,PDO::PARAM_STR);
        $stat->execute();
    }

    public function adminModifyUser(UserUpdate $user): void
    {
        $loginEnabled = $user->isLoginEnabled();
        $email = $user->getEmail();
        $id = $user->getId();
        $stat = parent::getInstance()->connect()->prepare(
            'UPDATE users SET email = :email, login_enabled = :bool WHERE id = :id');
        $stat->bindParam(':email',$email,PDO::PARAM_STR);
        $stat->bindParam(':bool',$loginEnabled,PDO::PARAM_BOOL);
        $stat->bindParam(':id',$id,PDO::PARAM_INT);
        $stat->execute();


        $stat = parent::getInstance()->connect()->prepare(
            'DELETE FROM users_types WHERE id_users = ?');
        $stat->execute([
            $user->getId()
        ]);

        $stat = parent::getInstance()->connect()->prepare(
            'INSERT INTO users_types (id_users, id_account_type) VALUES (?,?)');
        if(isset($user->getRights()[0]))
        {
            foreach ($user->getRights() as $right) {
                $stat->execute([
                    $user->getId(),
                    $this->getRightId($right)
                ]);
            }
        }
    }




    public function getRightId(string $name): int
    {
        $stat = parent::getInstance()->connect()->prepare(
            'SELECT id FROM account_type where name= :name
        ');
        $stat->bindParam(':name',$name,PDO::PARAM_STR);
        $stat->execute();
        $items = $stat->fetch(PDO::FETCH_ASSOC);
        return $items['id'];
    }

    public function getUserDetailsId(UserRegistering $user): int
    {
        $name = $user->getFirstname();
        $surname = $user->getSurname();
        $stat = parent::getInstance()->connect()->prepare('
            SELECT id FROM user_details WHERE name = :name AND surname = :surname
        ');
        $stat->bindParam(':surname', $surname, PDO::PARAM_STR);
        $stat->bindParam(':name', $name, PDO::PARAM_STR);
        $stat->execute();

        $data = $stat->fetch(PDO::FETCH_ASSOC);
        return $data['id'];
    }



    public function getDBUsers()
    {
        $stat = parent::getInstance()->connect()->prepare(
            'SELECT u.id id, u.email email, u.login_enabled login_enabled,
                            d.id detid, d.name firstname, d.surname surname, d.phone phone 
                            FROM users u 
                            JOIN user_details d ON u.id_user_details = d.id
        ');
        $stat->execute();
        $items = $stat->fetchAll(PDO::FETCH_ASSOC);
        $result = [];
        foreach ($items as $item){
            $result[] = new UserFull(
                $item['email'],
                $item['login_enabled'],
                $item['id'],
                $item['detid'],
                $item['firstname'],
                $item['surname'],
                $item['phone']
            );
        }
        return $result;
    }

    public function deleteUser($id, $detid)
    {
        $stat = parent::getInstance()->connect()->prepare('DELETE FROM users WHERE id= :id');
        $stat->bindParam(':id',$id,PDO::PARAM_STR);
        $stat->execute();
    }



}