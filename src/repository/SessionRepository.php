<?php

require_once 'Repository.php';

class SessionRepository extends Repository
{
    private string $cookieId;
    public function __construct(string $id)
    {
        parent::getInstance();
        $this->cookieId = $id;
    }

    public function login(int $userID): void
    {
        $stat = parent::getInstance()->connect()->prepare('
            INSERT INTO session (user_id, session_id) 
            VALUES (?,?)
        ');
        $stat->execute([
            $userID,
            $this->cookieId
        ]);
    }

    public function logout(): void
    {
        $stat = parent::getInstance()->connect()->prepare('
            DELETE FROM session WHERE session_id = ?
        ');
        $stat->execute([
            $this->cookieId
        ]);
    }

    public function checkCookie(): bool
    {
        $stat = parent::getInstance()->connect()->prepare('
            SELECT exists(SELECT 1 FROM session WHERE session_id = ?) AS present
        ');
        $stat->execute([
            $this->cookieId
        ]);
        $ans = $stat->fetch(PDO::FETCH_ASSOC);
        return $ans['present'];
    }

    public function getUserId(): ?int
    {
        $stat = parent::getInstance()->connect()->prepare('
            SELECT user_id FROM session WHERE session_id = ?
        ');
        $stat->execute([
            $this->cookieId
        ]);
        $ans = $stat->fetch(PDO::FETCH_ASSOC);
        if($ans == false)
            return null;
        return $ans['user_id'];
    }

    public function killSessionByUserID(int $id): void
    {
        $stat = parent::getInstance()->connect()->prepare('
            DELETE FROM session WHERE user_id = ?
        ');
        $stat->execute([
            $id
        ]);
    }
}