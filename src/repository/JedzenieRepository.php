<?php

require_once 'Repository.php';
require_once __DIR__ . '/../models/Jedzenie.php';
class JedzenieRepository extends Repository
{
    public function __construct()
    {
        parent::getInstance();
    }

    public function addJedzenie(string $nazwa, int $jednostkaMiary): void
    {
        //TODO
        $stat = parent::getInstance()->connect()->prepare('
            INSERT INTO jedzenie (id_jednostka_miary, nazwa)
            VALUES (?, ?)
        ');
        $stat->execute([
            $jednostkaMiary,
            $nazwa
        ]);
    }

    public function modifyJedzenie(Jedzenie $jedzenie): void
    {
        $stat = parent::getInstance()->connect()->prepare(
            'UPDATE jedzenie SET nazwa = ?, id_jednostka_miary = ? WHERE id = ?');
        $stat->execute([
            $jedzenie->getNazwa(),
            $jedzenie->getJednMiary(),
            $jedzenie->getId()
        ]);
    }

    public function deleteJedzenie($id): void
    {
        $stat = parent::getInstance()->connect()->prepare('
            DELETE FROM jedzenie WHERE id = :id
        ');
        $stat->bindParam(':id',$id,PDO::PARAM_INT);
        $stat->execute();
    }

    public function getJedzenie(): ?array
    {
        $result = [];
        $stat = parent::getInstance()->connect()->prepare(
            'SELECT * FROM jedzenie');
        $stat->execute();

        $items = $stat->fetchAll(PDO::FETCH_ASSOC);

        foreach ($items as $jedzenie){
            $result[] = new Jedzenie(
                $jedzenie['id'],
                $jedzenie['nazwa'],
                $jedzenie['id_jednostka_miary']
            );
        }
        return $result;
    }

}