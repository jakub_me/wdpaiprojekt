<?php

require_once 'Repository.php';

class PrivilegesRepository extends Repository
{
    public function __construct()
    {
        parent::getInstance();
    }

    public function getRights(string $email): ?array
    {
        $stat = parent::getInstance()->connect()->prepare(
            'SELECT a.name x FROM users u WHERE email = :email and login_enabled = true 
JOIN users_types t ON u.id = t.id_users
JOIN account_type a ON a.id = t.id_account_type
        ');
        $stat->bindParam(':email',$email,PDO::PARAM_STR);
        $stat->execute();
        $items = $stat->fetchAll(PDO::FETCH_ASSOC);
        $result = [];
        foreach ($items as $item)
            $result[] = $item['x'];
        return $result;
    }

    public function getRightsList(): array
    {
        $stat = parent::getInstance()->connect()->prepare(
            'SELECT id, name FROM account_type
        ');
        $stat->execute();
        $items = $stat->fetchAll(PDO::FETCH_ASSOC);
        $result = [];
        foreach ($items as $item)
            $result[] = new Rights(
                $item['id'],
                $item['name']
            );
        return $result;
    }




    public function getUserRightsAllUsers(): ?array
    {
        $result = [];
        $stat = parent::getInstance()->connect()->prepare(
            "SELECT string_agg(a.name, '!') arr, u.id_users uid 
                    FROM users_types u
                    JOIN account_type a 
                    ON u.id_account_type = a.id
                    GROUP BY uid");
        $stat->execute();

        $items = $stat->fetchAll(PDO::FETCH_ASSOC);
        if($items == false)
            return null;
        foreach ($items as $item) {
            $result[$item['uid']] = explode('!',$item['arr']);
        }
        return $result;
    }



    public function getUserRightsById(int $id): ?array
    {
        $stat = parent::getInstance()->connect()->prepare(
            'SELECT arr FROM vconcatrights
                    WHERE uzyt = :id');
        $stat->bindParam(':id',$id,PDO::PARAM_INT);
        $stat->execute();

        $items = $stat->fetch(PDO::FETCH_ASSOC);
        if($items['arr'] == false)
            return null;
        return explode('!',$items['arr']);
    }



    public function getUserRightsByEmail(string $email): ?array
    {
        $stat = parent::getInstance()->connect()->prepare(
            "SELECT string_agg(a.name, '!') arr FROM users u
                    JOIN users_types ut 
                    ON u.id = ut.id_users
                    JOIN account_type a 
                    ON ut.id_account_type = a.id
                    WHERE u.email = :email
                    GROUP BY u.email");
        $stat->bindParam(':email',$email,PDO::PARAM_STRING);
        $stat->execute();

        $items = $stat->fetch(PDO::FETCH_ASSOC);
        if($items['arr'] == false)
            return null;
        return explode('!',$items['arr']);
    }
}