<?php

require_once 'Repository.php';
require_once __DIR__ . '/../models/JednostkaMiary.php';

class JednostkaRepository extends Repository
{
    public function __construct()
    {
        parent::getInstance();
    }

    public function addJednostka(string $nazwa)
    {
        $stat = parent::getInstance()->connect()->prepare('
            INSERT INTO jednostka_miary (nazwa)
            VALUES (?)
        ');
        $stat->execute([
            $nazwa
        ]);
    }

    public function modifyJednostka(JednostkaMiary $j)
    {
        $nazwa = $j->getNazwa();
        $id = $j->getId();
        $stat = parent::getInstance()->connect()->prepare('
            UPDATE jednostka_miary SET nazwa=:nazwa WHERE id = :id
        ');
        $stat->bindParam(':nazwa',$nazwa,PDO::PARAM_STR);
        $stat->bindParam(':id',$id,PDO::PARAM_INT);
        $stat->execute();
    }
    public function deleteJednostka(int $id)
    {
        $stat = parent::getInstance()->connect()->prepare('
            DELETE FROM jednostka_miary WHERE id = :id
        ');
        $stat->bindParam(':id',$id,PDO::PARAM_INT);
        $stat->execute();
    }
    public function getJednostka(string $nazwa): ?JednostkaMiary
    {
        $stat = parent::getInstance()->connect()->prepare('
            SELECT * FROM jednostka_miary WHERE nazwa= :nazwa
        ');
        $stat->bindParam(':nazwa',$nazwa,PDO::PARAM_STR);
        $stat->execute();

        $j = $stat->fetch(PDO::FETCH_ASSOC);
        if($j == false)
            return null;

        return new JednostkaMiary(
            $j['id'],
            $j['nazwa'],
        );
    }
    public function getJednostki(): ?array
    {
        $stat = parent::getInstance()->connect()->prepare('
            SELECT * FROM jednostka_miary
        ');
        $stat->execute();

        $j = $stat->fetchAll(PDO::FETCH_ASSOC);
        $result = [];
        foreach ($j as $item){
            $result[] = new JednostkaMiary(
                $item['id'],
                $item['nazwa']
            );
        }
        return $result;
    }
}