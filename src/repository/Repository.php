<?php

require_once __DIR__.'/../../Database.php';
class Repository
{
    private static ?Database $database = null;
    private function __construct(){}
    public static function getInstance()
    {
        if (self::$database === null)
            self::$database = new Database();
        return self::$database;
    }
}
