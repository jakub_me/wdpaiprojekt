<?php


class UserPhone
{
    private int $id;
    private ?string $phone;

    public function __construct(int $id, ?string $phone=null)
    {
        $this->id = $id;
        $this->phone = $phone;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }
    public function setPhone(?string $phone): void
    {
        $this->phone = $phone;
    }

}