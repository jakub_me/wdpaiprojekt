<?php


class UserFull
{
    private string $username;
    //NO PASSWORD - FOR SECURITY!!!
    //NO SALT - FOR SECURITY!!!
    private bool $login_enabled;
    private int $id;
    private int $idUserDetails;
    private string $firstname;
    private string $surname;
    private ?string $phone;

    public function __construct(string $username, bool $login_enabled, int $id,
                                int $idUserDetails, string $firstname,
                                string $surname, ?string $phone = '')
    {
        $this->username = $username;
        $this->login_enabled = $login_enabled;
        $this->id = $id;
        $this->idUserDetails = $idUserDetails;
        $this->firstname = $firstname;
        $this->surname = $surname;
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    /**
     * @return bool
     */
    public function isLoginEnabled(): bool
    {
        return $this->login_enabled;
    }

    /**
     * @param bool $login_enabled
     */
    public function setLoginEnabled(bool $login_enabled): void
    {
        $this->login_enabled = $login_enabled;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getIdUserDetails(): int
    {
        return $this->idUserDetails;
    }

    /**
     * @param int $idUserDetails
     */
    public function setIdUserDetails(int $idUserDetails): void
    {
        $this->idUserDetails = $idUserDetails;
    }

    /**
     * @return string
     */
    public function getFirstname(): string
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     */
    public function setFirstname(string $firstname): void
    {
        $this->firstname = $firstname;
    }

    /**
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname(string $surname): void
    {
        $this->surname = $surname;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     */
    public function setPhone(?string $phone): void
    {
        $this->phone = $phone;
    }


}