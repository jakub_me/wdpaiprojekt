<?php


class MenuItem
{
    private int $id;        //input name (letter+id)
    private string $nazwa;
    private int $cena;
    private string $skladniki;
    private bool $enabled;

    //needed in koszyk, not needed in Menu

    public function __construct(int $id, string $nazwa, int $cena, string $skladniki, bool $enabled=true)
    {
        $this->id = $id;
        $this->nazwa = $nazwa;
        $this->cena = $cena;
        $this->skladniki = ucfirst($skladniki);
        $this->enabled = $enabled;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getNazwa(): string
    {
        return $this->nazwa;
    }

    public function setNazwa(string $nazwa): void
    {
        $this->nazwa = $nazwa;
    }

    public function getCena(): int
    {
        return $this->cena;
    }

    public function setCena(int $cena): void
    {
        $this->cena = $cena;
    }

    public function getSkladniki(): string
    {
        return $this->skladniki;
    }

    public function setSkladniki(string $skladniki): void
    {
        $this->skladniki = $skladniki;
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): void
    {
        $this->enabled = $enabled;
    }

}