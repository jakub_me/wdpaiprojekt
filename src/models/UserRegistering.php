<?php


class UserRegistering
{
    private string $username;
    private string $password;
    private string $salt;
    private string $firstname;
    private string $surname;
    private ?string $phone;

    public function __construct(string $username, string $password, string $salt, string $firstname, string $surname, ?string $phone = null)
    {
        $this->username = $username;
        $this->password = $password;
        $this->salt = $salt;
        $this->firstname = $firstname;
        $this->surname = $surname;
        $this->phone = $phone;
    }


    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    public function getSalt(): string
    {
        return $this->salt;
    }

    public function setSalt(string $salt): void
    {
        $this->salt = $salt;
    }

    public function getFirstname(): string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): void
    {
        $this->firstname = $firstname;
    }

    public function getSurname(): string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): void
    {
        $this->surname = $surname;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): void
    {
        $this->phone = $phone;
    }
}