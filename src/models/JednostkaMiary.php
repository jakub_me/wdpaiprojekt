<?php


class JednostkaMiary
{
    private int $id;
    private string $nazwa;

    public function __construct(int $id, string $nazwa)
    {
        $this->id = $id;
        $this->nazwa = $nazwa;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getNazwa(): string
    {
        return $this->nazwa;
    }

    public function setNazwa(string $nazwa): void
    {
        $this->nazwa = $nazwa;
    }
}