<?php


class Skladnik
{
    private int $idJedz;
    private int $idMenu;
    private int $ilosc;

    /**
     * Skladnik constructor.
     * @param int $idJedz
     * @param int $idMenu
     * @param int $ilosc
     */
    public function __construct(int $idJedz, int $idMenu, int $ilosc)
    {
        $this->idJedz = $idJedz;
        $this->idMenu = $idMenu;
        $this->ilosc = $ilosc;
    }

    /**
     * @return int
     */
    public function getIdJedz(): int
    {
        return $this->idJedz;
    }

    /**
     * @param int $idJedz
     */
    public function setIdJedz(int $idJedz): void
    {
        $this->idJedz = $idJedz;
    }

    /**
     * @return int
     */
    public function getIdMenu(): int
    {
        return $this->idMenu;
    }

    /**
     * @param int $idMenu
     */
    public function setIdMenu(int $idMenu): void
    {
        $this->idMenu = $idMenu;
    }

    /**
     * @return int
     */
    public function getIlosc(): int
    {
        return $this->ilosc;
    }

    /**
     * @param int $ilosc
     */
    public function setIlosc(int $ilosc): void
    {
        $this->ilosc = $ilosc;
    }
}