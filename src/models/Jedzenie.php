<?php


class Jedzenie
{
    private int $id;
    private string $nazwa;
    private int $jednMiary;

    public function __construct(int $id, string $nazwa, int $jednMiary)
    {
        $this->id = $id;
        $this->nazwa = $nazwa;
        $this->jednMiary = $jednMiary;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getNazwa(): string
    {
        return $this->nazwa;
    }

    public function setNazwa(string $nazwa): void
    {
        $this->nazwa = $nazwa;
    }

    public function getJednMiary(): int
    {
        return $this->jednMiary;
    }

    public function setJednMiary(int $jednMiary): void
    {
        $this->jednMiary = $jednMiary;
    }
}