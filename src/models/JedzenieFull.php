<?php


class JedzenieFull
{
    private int $id;
    private string $nazwa;
    private string $jednNazwa;

    public function __construct(int $id, string $nazwa, string $jednNazwa)
    {
        $this->id = $id;
        $this->nazwa = $nazwa;
        $this->jednNazwa = $jednNazwa;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getJednNazwa(): string
    {
        return $this->jednNazwa;
    }

    public function setJednNazwa(string $jednNazwa): void
    {
        $this->jednNazwa = $jednNazwa;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getNazwa(): string
    {
        return $this->nazwa;
    }

    public function setNazwa(string $nazwa): void
    {
        $this->nazwa = $nazwa;
    }
}