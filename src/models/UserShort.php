<?php


class UserShort
{
    private int $id;
    private string $name;
    private string $password;
    private string $salt;
    private bool $login_enabled;

    public function isLoginEnabled(): bool
    {
        return $this->login_enabled;
    }

    public function setLoginEnabled(bool $login_enabled): void
    {
        $this->login_enabled = $login_enabled;
    }

    public function getSalt(): string
    {
        return $this->salt;
    }

    public function setSalt(string $salt): void
    {
        $this->salt = $salt;
    }

    public function __construct(string $name, string $password, string $salt, bool $login_enabled, int $id)
    {
        $this->name = $name;
        $this->password = $password;
        $this->salt = $salt;
        $this->login_enabled = $login_enabled;
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name): void
    {
        $this->name = $name;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password): void
    {
        $this->password = $password;
    }
}