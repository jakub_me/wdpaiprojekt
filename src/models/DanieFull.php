<?php


class DanieFull
{
    private string $nazwa;
    private int $cena;
    private int $id;
    private bool $enabled;

    public function __construct(string $nazwa, int $cena, int $id, bool $enabled)
    {
        $this->nazwa = $nazwa;
        $this->cena = $cena;
        $this->id = $id;
        $this->enabled = $enabled;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): void
    {
        $this->enabled = $enabled;
    }


    public function getNazwa(): string
    {
        return $this->nazwa;
    }

    public function setNazwa(string $nazwa): void
    {
        $this->nazwa = $nazwa;
    }

    public function getCena(): int
    {
        return $this->cena;
    }

    public function setCena(int $cena): void
    {
        $this->cena = $cena;
    }
}