<?php


class Danie
{
    private string $nazwa;
    private int $cena;

    public function __construct(string $nazwa, int $cena)
    {
        $this->nazwa = $nazwa;
        $this->cena = $cena;
    }

    public function getNazwa(): string
    {
        return $this->nazwa;
    }

    public function setNazwa(string $nazwa): void
    {
        $this->nazwa = $nazwa;
    }

    public function getCena(): int
    {
        return $this->cena;
    }

    public function setCena(int $cena): void
    {
        $this->cena = $cena;
    }
}