<?php


class UserUpdate
{
    private int $id;
    private string $email;
    private bool $loginEnabled;
    private ?array $rights;
    public function __construct(int $id, string $email, bool $loginEnabled, ?array $rights = null)
    {
        $this->id = $id;
        $this->email = $email;
        $this->loginEnabled = $loginEnabled;
        $this->rights = $rights;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function isLoginEnabled(): bool
    {
        return $this->loginEnabled;
    }

    public function setLoginEnabled(bool $loginEnabled): void
    {
        $this->loginEnabled = $loginEnabled;
    }

    public function getRights(): ?array
    {
        return $this->rights;
    }

    public function setRights(?array $rights): void
    {
        $this->rights = $rights;
    }

}