<?php


class UserInfo
{
    private string $firstname;
    private string $surname;
    private ?string $phone;

    /**
     * UserFull constructor.
     * @param string $firstname
     * @param string $surname
     * @param string|null $phone
     */
    public function __construct(string $firstname, string $surname, ?string $phone)
    {
        $this->firstname = $firstname;
        $this->surname = $surname;
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getFirstname(): string
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     */
    public function setFirstname(string $firstname): void
    {
        $this->firstname = $firstname;
    }

    /**
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname(string $surname): void
    {
        $this->surname = $surname;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     */
    public function setPhone(?string $phone): void
    {
        $this->phone = $phone;
    }

}