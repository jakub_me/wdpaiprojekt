<?php

require_once "src/controllers/SecurityController.php";
require_once "src/controllers/MenuController.php";
require_once "src/controllers/KoszykController.php";
require_once "src/controllers/DBJednostkaController.php";
require_once "src/controllers/DBJedzenieController.php";
require_once "src/controllers/DBUsersController.php";
require_once "src/controllers/DBMenuController.php";
require_once "src/controllers/DBController.php";
require_once "src/controllers/PanelController.php";
require_once "src/controllers/HomepageController.php";

class Routing{
    public static $routes;

    public static function get($url, $controller){
        self::$routes[$url] = $controller;
    }

    public static function post($url, $controller){
        self::$routes[$url] = $controller;
    }
    public static function run($url){
        $action = explode("/", $url)[0];
        if(!array_key_exists($action, self::$routes)){
            die("Wrong url!");
        }
        $controller = self::$routes[$action]; //tu jest string
        /*if (!$controller)
            $controller="main1";*/
        $object = new $controller; //tworzenie z nazwy w stringu
        $action = $action?: 'main1'; //strona glowna - bez podawania jej nazwy
        $object->$action();
    }
}