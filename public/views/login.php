<!DOCTYPE HTML>
<head>
	<title>HOME PAGE</title>
	<link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;1,400&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="public/css/common.css">
	<link rel="stylesheet" type="text/css" href="public/css/style-login-common.css">
	<script type="text/javascript" src="public/js/validate/login.js" defer></script>
</head>
<body>
	<div class="logo">
		<img src="public/img/food.jpg">
		<h1>FOOD :)</h1>
	</div>
	<div class="content">
		<form class="btlist" action="login" method="POST">
			<input name="email" class="bt button2" type="text" placeholder="LOGIN">
			<input name="password" class="bt button2" type="password" placeholder="HASŁO">
			<button name="login" type="submit" class="bt button1">ZALOGUJ</button>
            <a href="register" class="bt button1">REJESTRACJA</a>
            <a href="/" class="bt button1">POWRÓT</a>
		</form>
        <p id="errtext"><?php if(isset($messages)){
                foreach($messages as $message)
                    echo $message;
            }
            ?></p>
    </div>
</body>

 