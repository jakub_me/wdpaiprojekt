<!DOCTYPE HTML>
<head>
	<title>HOME PAGE</title>
	<link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;1,400&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="public/css/common.css">
	<link rel="stylesheet" type="text/css" href="public/css/style-login-common.css">
</head>
<body>
	<div class="logo<?php if($img) echo ' computer'; ?>">
		<img src="public/img/food.jpg">
		<h1>FOOD :)</h1>
	</div>
	<div class="content">
		<h2 <?php if($h2) echo 'class="computer"'; ?>><?php echo $title;?></h2>
		<ul class="btlist">
            <?php foreach ($buttons as $button) {
                echo $button;
            } ?>
		</ul>

    </div>
</body>

 