<!DOCTYPE HTML>
<head>
	<title>Menu</title>
	<link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;1,400&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="public/css/common.css">
	<link rel="stylesheet" type="text/css" href="public/css/style-menu.css">
    <script type="text/javascript" src="public/js/koszyk/menu.js" defer></script>
</head>
<body>
		    <h2 id="title">Menu</h2>
            <section>

            <?php foreach ($dania as $d): ?>
                <div class="record phone">
                    <div class="mainleft">
                        <div class="up">
                            <div class="left"><?= $d->getNazwa(); ?></div>
                            <div class="down"><?= $d->getSkladniki(); ?></div>
                            <div class="middle">
                                <?= number_format(0.01*$d->getCena(),2) ?>zł za porcję
                            </div>
                        </div>
                    </div>
                    <div class="mainright">
                        <div class="right"><input name="<?= $d->getId(); ?>" class="right" type="number"></div>
                        <button type="button" class="add" name="id" value="<?= $d->getId(); ?>">+</button>
                    </div>
                </div>
                <div class="record computer">
                    <div class="mainleft">
                        <div class="up">
                            <div class="left"><?= $d->getNazwa(); ?></div>
                            <div class="middle">
                                <?= number_format(0.01*$d->getCena(),2) ?>zł za porcję
                            </div>
                            <div class="right"><input name="<?= $d->getId(); ?>" class="right" type="number"></div>
                        </div>
                        <div class="down"><?= $d->getSkladniki(); ?></div>
                    </div>
                    <div class="mainright">
                        <button type="button" class="add" name="id" value="<?= $d->getId(); ?>">+</button>
                    </div>
                </div>
            <?php endforeach; ?>
            </section>
            <footer class=".btcomputer">
                <a class="bta button1" href="koszyk">DO KOSZYKA</a>
                <a class="bta button1" href="/">POWRÓT</a>
            </footer>
        <a class="bta button1 phone" href="koszyk">DO KOSZYKA</a>
        <a class="bta button1 phone" href="/">POWRÓT</a>
            <p id="errtext"><?php if(isset($messages)){
                    foreach($messages as $message)
                        echo $message;
                }
                ?></p>

</body>

 