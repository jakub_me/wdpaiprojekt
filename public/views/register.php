<!DOCTYPE HTML>
<head>
	<title>HOME PAGE</title>
	<link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;1,400&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="public/css/common.css">
	<link rel="stylesheet" type="text/css" href="public/css/style-login-common.css">
    <script type="text/javascript" src="public/js/validate/register.js" defer></script>
</head>
<body>
	<div class="logo computer">
		<img src="public/img/food.jpg">
		<h1>FOOD :)</h1>
	</div>
	<div class="content">
		<h2 id="title">Rejestracja</h2>
		<form class="btlist" action="newUser" method="POST">
            <input name="email" class="bt button2" type="text" placeholder="EMAIL">
            <input name="password" class="bt button2" type="password" placeholder="HASŁO">
            <input name="confirmPassword" class="bt button2" type="password" placeholder="POWTÓRZ HASŁO">
            <input name="firstname" class="bt button2" type="text" placeholder="IMIĘ">
            <input name="surname" class="bt button2" type="text" placeholder="NAZWISKO">
            <input name="phone" class="bt button2" type="text" placeholder="NUMER TELEFONU (opcjonalne)">
            <button name="register" type="submit" class="bt button1">REJESTRACJA</button>
            <a href="/" class="bt button1">DO STRONY GŁÓWNEJ</a>
            <!--<input name="rejestracja" type="password" placeholder="HASŁO">
            <input name="zaloguj" type="password" placeholder="HASŁO">-->
			<!--<li><a class="bta button2" href="login.php">WYLOGUJ</a></li>-->
		</form>
        <p id="errtext"><?php if(isset($messages)){
                foreach($messages as $message)
                    echo $message;
            }
            ?></p>
    </div>
</body>

 