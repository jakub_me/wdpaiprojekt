<!DOCTYPE HTML>
<head>
	<title>Baza - tabela jedzenie</title>
	<link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;1,400&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="public/css/common.css">
	<link rel="stylesheet" type="text/css" href="public/css/style-base-menu.css">
    <script type="text/javascript" src="public/js/basemenu.js" defer></script>
</head>
<body>
    <div id="left">
        <h2 id="title">Widok dania (menu)</h2>
        <section>
            <?php foreach ($dania as $d):?>
            <div class="recordDanie">
                <div class="mainleft" id="r<?= $d->getId(); ?>">
                    <div class="up">
                        <div class="left"><?= $d->getNazwa(); ?></div>
                        <div class="middle"><?= number_format(0.01*$d->getCena(),2) ?>zł</div>
                    </div>
                    <div class="down"><?= $d->getSkladniki(); ?></div>
                </div>
                <div class="mainright">
                    <form method="post" action="deleteDanie">
                        <input type="hidden" name="danieId" value="<?= $d->getId(); ?>">
                        <button type="submit" name="remove">X</button>
                    </form>
                </div>
            </div>
            <?php endforeach; ?>
        </section>
    </div>

    <div id="right">
        <form action="manageMenu" method="post">
            <input type="text" placeholder="nazwa nowego dania lub edycja" name="text">
            <label>Cena za porcję w gr:<input id="cena" type="number" name="cena"></label>
            <div class="record">
                Dodawanie nowych składników:
            </div>
            <article>
                <?php foreach ($jedzenia as $j): ?>
                <div class="recordAdd">
                    <label> <?= $j->getNazwa(); ?>
                        <input type="checkbox" name="ids[]" value="<?= $j->getId(); ?>">
                    </label>
                    <input type="number" value="0" name="n<?=$j->getId();?>">
                    <p><?=$j->getJednNazwa();?></p>
                </div>
                <?php endforeach; ?>
            </article>

            <div class="record">
                Edycja (>0) i usuwanie (=0) składników
            </div>
            <input id="menuid" type="hidden" name="menuid" value="-1">
            <div id="modifySection">

            </div>
            <ul class="btlist">
                <button type="submit" name="new" class="addNew">!!!!UTWÓRZ NOWE DANIE</button>
                <button type="submit" name="edit" class="edit" disabled>!!!!!AKTUALIZUJ WYBRANE DANIE</button>
                <li><a class="bta button1" href="dbpanel">POWRÓT</a></li>
            </ul>
        </form>
        <p id="errtext"><?php if(isset($messages)){
                foreach($messages as $message)
                    echo $message;
            }
            ?></p>
    </div>
</body>

<template id="modify-template">
    <div class="recordModify">
        <p class="jedzenieName"></p>
        <input type="number">
        <p class="jednostka"></p>
        <input type="hidden" name="modifyid[]">
    </div>
</template>