<!DOCTYPE HTML>
<head>
	<title>Zmiana hasła</title>
	<link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;1,400&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="public/css/common.css">
	<link rel="stylesheet" type="text/css" href="public/css/style-login-common.css">
    <link rel="stylesheet" type="text/css" href="public/css/style-passchange.css">
    <script type="text/javascript" src="public/js/validate/passchange.js" defer></script>
</head>
<body>
	<div class="logo computer">
		<img src="public/img/food.jpg">
		<h1>FOOD :)</h1>
	</div>
	<div class="content">
		<h2>Zmiana hasła</h2>
		<form class="btlist" action="changePhone" method="post">
            <p class="bt buttonShaded">
                <?= $user->getFirstName().' '.$user->getSurname(); ?>
            </p>
            <input name="phone" class="bt button2" type="text" value="<?= $user->getPhone(); ?>" placeholder="NUMER TELEFONU">
            <button class="bt button1" name="changePhone">ZATWIERDŹ NUMER</button>
		</form>
        <form class="btlist" action="changePass" method="post">
			<input name="oldpass" class="bt button2" type="password" placeholder="STARE HASŁO">
			<input name="newpass" class="bt button2" type="password" placeholder="NOWE HASŁO">
            <input name="newpassConfirmed" class="bt button2" type="password" placeholder="POWTÓRZ NOWE HASŁO">
			<button class="bt button1" name="changePass">ZATWIERDŹ HASŁO</button>
			<a href="panel" class="bt button1">POWRÓT</a>
		</form>
		<p id="errtext"><?php if(isset($messages)){
                foreach($messages as $message)
                    echo $message;
            }
            ?></p>
	</div>	
</body>

 