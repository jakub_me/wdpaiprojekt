<!DOCTYPE HTML>
<head>
	<title>Baza - tabela jedzenie</title>
	<link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;1,400&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="public/css/common.css">
	<link rel="stylesheet" type="text/css" href="public/css/style-base-users.css">
</head>
<body>
    <h2 id="title">Widok users</h2>
    <section>
        <!--<div class="searchBar" action="searchUsersByRights" method="post">
            <div>Wyszukiwanie po prawach:</div>
            <?php foreach ($rights as $r): ?>
            <label><?= $r->getName() ?>
                <input type="checkbox" name="searchRights" value="<?= $r->getId() ?>">
            </label>
            <?php endforeach; ?>
            <button type="submit" class="search">wyszukaj</button>
            <button type="submit" class="cancelSearch">X</button>
        </div>-->
        <div class="record">
            Modyfikacja rekordów:
        </div>
        <?php foreach ($users as $u): ?>
        <form class="record" action="modifyUser" method="post">
            <div class="mainleft">
                <div class="up">
                    <?= $u->getFirstname().' '.$u->getSurname().' '.$u->getPhone(); ?>
                </div>
                <div class="middle">
                    <label>enabled
                        <input type="checkbox" name="login_enabled" <?php if($u->isLoginEnabled()==true) echo "checked"; ?>>
                    </label>

                    <input type="hidden" name="id" value="<?= $u->getId()?>">
                    <input type="hidden" name="id_user_details" value="<?= $u->getIdUserDetails()?>">
                    <input type="text" name="text" value="<?= $u->getUsername()?>">
                </div>
                <div class="down">
                    <?php foreach ($rights as $r): ?>
                        <label> <?= $r->getName(); ?>
                            <input type="checkbox" name="rights[]" value="<?= $r->getName(); ?>" <?php
                                if(!is_null($types[$u->getId()]) && in_array($r->getName(),$types[$u->getId()]))
                                    echo "checked"; ?>>
                        </label>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="mainright">
                <button name="edit" class="edit">-></button>
                <button name="rem" class="rem">X</button>
            </div>
        </form>
        <?php endforeach; ?>

    </section>
    <ul class="btlist">
        <li><a class="bta button1" href="dbpanel">POWRÓT</a></li>
    </ul>
            <p id="errtext"><?php if(isset($messages)){
                    foreach($messages as $message)
                        echo $message;
                }
                ?></p>

</body>

 