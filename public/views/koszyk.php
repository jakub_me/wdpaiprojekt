<!DOCTYPE HTML>
<head>
	<title>Menu</title>
	<link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;1,400&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="public/css/common.css">
	<link rel="stylesheet" type="text/css" href="public/css/style-koszyk.css">
    <script type="text/javascript" src="public/js/koszyk/koszyk.js" defer></script>
</head>
<body>

    <form class="content" action="koszykBuy" method="post" enctype="multipart/form-data">
		<h2 id="title">Koszyk</h2>
		<section>
            <?php foreach ($koszyk as $d): ?>
            <div class="record">
                <div class="mainleft">
                    <div class="left"><?=$d->getNazwa(); ?></div>
                    <div class="middle">
                        <?= $count[$d->getId()].'x'.number_format(0.01*$d->getCena(),2) ?>zł za porcję
                    </div>
                </div>
                <input type="hidden" name="ilosc<?=$d->getId(); ?>" value="<?=$count[$d->getId()]; ?>">
                <input type="hidden" name="id[]" value="<?=$d->getId(); ?>">
                <button class="remove" type="submit">x</button>
            </div>
            <?php endforeach; ?>
		</section>
        <footer>
            <div id="row1" class="btlist">
                <p id="topay" class="bta">
                    Do zapłaty: <?= number_format(0.01*$cena,2) ?>zł
                </p>
                <div id="fileinput" class="bta button2">
                    <input class="bta" type="file" name="file">
                    <div id="buildingphoto" class="bta button2">(opcjonalne - dla dostawcy)<br>załącz zdjęcie budynku</div>
                </div>
            </div>
            <div id="row2" class="btlist">
                <input id="address" class="bta button" type="text" name="address"
                       placeholder="adres (jeśli na miejscu - zostaw puste)">
                <button type="button" class="bta button1" id="rem">WYCZYŚĆ KOSZYK</button>
            </div>
            <div id="row3" class="btlist">
                <button type="submit" class="bta button1" name="accept">!!!!!ZAMÓW</button>
                <a href="menu" class="bta button1">POWRÓT DO MENU</a>
            </div>

            <p id="errtext"><?php if(isset($messages)){
                    foreach($messages as $message)
                        echo $message;
                }
                ?></p>
        </footer>
    </form>
</body>

 