<!DOCTYPE HTML>
<head>
	<title>Baza - tabela jednostki</title>
	<link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;1,400&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="public/css/common.css">
	<link rel="stylesheet" type="text/css" href="public/css/style-base-jednostki.css">
</head>
<body>
    <h2 id="title">Tabela jednostki</h2>
    <section>
        <form method="post" class="record" action="addJednostka">
            <input type="text" placeholder="dodaj nowy rekord" name="text">
            <button name="addRecord" class="add">+</button>
        </form>
        <div class="record">
            Modyfikacja rekordów:
        </div>
        <?php foreach ($items as $item): ?>
        <form method="post" class="record" action="modifyJednostka">
            <input type="hidden" name="id" value="<?= $item->getId(); ?>">
            <input type="text" name="text" value="<?= $item->getNazwa(); ?>">
            <button name="edit" class="edit" type="submit">-></button>
            <button name="rem" class="rem" type="submit">X</button>
        </form>
        <?php endforeach; ?>

    </section>
    <ul class="btlist">
        <li><a class="bta button1" href="dbpanel">POWRÓT</a></li>
    </ul>

            <p id="errtext"><?php if(isset($messages)){
                    foreach($messages as $message)
                        echo $message;
                }
                ?></p>

</body>

 