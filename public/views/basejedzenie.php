<!DOCTYPE HTML>
<head>
	<title>Baza - tabela jedzenie</title>
	<link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;1,400&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="public/css/common.css">
	<link rel="stylesheet" type="text/css" href="public/css/style-base-jedzenie.css">
</head>
<body>
    <h2 id="title">Tabela jedzenie</h2>
    <section>
        <form class="record" action="addJedzenie" method="post">
            <div class="up">
                <input type="text" placeholder="dodaj nowy rekord" name="text">
            </div>
            <div class="down">
                <div class="label">
                <?php foreach ($jednostki as $jednostka): ?>
                    <label> <?= $jednostka->getNazwa(); ?>
                        <input type="radio" name="jednostka" checked value="<?= $jednostka->getId(); ?>">
                    </label>
                <?php endforeach; ?>
                </div>
                <button name="addRecord" class="add" type="submit">+</button>
            </div>
        </form>
        <div class="record">
            Modyfikacja rekordów:
        </div>
        <?php foreach ($jedzenia as $j): ?>
        <form class="record" action="modifyJedzenie" method="post">
            <div class="up">
                <input type="hidden" name="id" value="<?= $j->getId()?>">
                <input type="text" name="text" value="<?= $j->getNazwa()?>">
            </div>
            <div class="down">
                <div class="label">
                <?php foreach ($jednostki as $jednostka): ?>
                    <label> <?= $jednostka->getNazwa(); ?>
                        <input type="radio" name="jednostka" value="<?= $jednostka->getId(); ?>" <?php
                        if($jednostka->getId() == $j->getJednMiary())
                            echo "checked"; ?>> </label>
                <?php endforeach; ?>
                </div>
                <button name="edit" class="edit">-></button>
                <button name="rem" class="rem">X</button>
            </div>

        </form>
        <?php endforeach; ?>

    </section>
    <ul class="btlist">
        <li><a class="bta button1" href="dbpanel">POWRÓT</a></li>
    </ul>
            <p id="errtext"><?php if(isset($messages)){
                    foreach($messages as $message)
                        echo $message;
                }
                ?></p>

</body>

 