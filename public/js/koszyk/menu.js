let dict = {};
const prevCookie = getCookie("koszyk");
if(prevCookie != "" && prevCookie != "{}"){
    dict = JSON.parse(prevCookie);
}

const inputs = document.querySelectorAll('input[type=number]');
const buttons = document.querySelectorAll('button.add');

function updateHiddenInput(actualInput, inputToUpdate){
    inputToUpdate.value = actualInput.value;
}

function buttonClick(val, input1, input2){
    if(parseInt(input1.value) > 0) {
        dict[val] = input1.value;
    }
    else{
        delete dict[val];
    }
    input1.value="0";
    input2.value="0";
    setCookie("koszyk",JSON.stringify(dict));
}

function getCookie(cname) {
    const name = cname + "=";
    const ca = document.cookie.split(';');
    for(let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function setCookie(name, val) {
    let d = new Date();
    d = new Date(d.getTime() + 1000 * 3600*24);
    document.cookie = name + '=' + val + '; expires=' + d.toGMTString() + ';';
}


for(let i = 0; i < inputs.length; i += 2)
{
    inputs[i].addEventListener('change', function (){
        updateHiddenInput(this, inputs[i+1])
    });
    inputs[i+1].addEventListener('change', function (){
        updateHiddenInput(this, inputs[i])
    });
    buttons[i].addEventListener('click',function (){
        buttonClick(this.value, inputs[i],inputs[i+1])
    })
    buttons[i+1].addEventListener('click',function (){
        buttonClick(this.value, inputs[i+1], inputs[i])
    })
}
