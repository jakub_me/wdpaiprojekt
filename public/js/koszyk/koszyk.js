let dict = {};
const prevCookie = getCookie("koszyk");
if(prevCookie != "" && prevCookie != "{}"){
    dict = JSON.parse(prevCookie);
}

const buttonsRemove = document.querySelectorAll('button.remove');
const section = document.querySelector('section');

function remove1(button){
    const id = parseInt(button.previousElementSibling.value);
    delete dict[id];
    setCookie("koszyk",JSON.stringify(dict));
    button.parentNode.parentNode.removeChild(button.parentNode);
}

function removeAll(){
    section.innerHTML="";
    setCookie("koszyk","{}");
    document.querySelector('button[name=accept]').setAttribute('disabled','disabled');
}


function getCookie(cname) {
    const name = cname + "=";
    const ca = document.cookie.split(';');
    for(let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function setCookie(name, val) {
    let d = new Date();
    d = new Date(d.getTime() + 1000 * 3600*24);
    document.cookie = name + '=' + val + '; expires=' + d.toGMTString() + ';';
}

for(let i = 0; i < buttonsRemove.length; i++)
{
    buttonsRemove[i].addEventListener('click', function (){
        remove1(this)
    });
}
document.querySelector('#rem').addEventListener('click', function (){
    removeAll()
});
