const chooseDanie = document.querySelectorAll(".mainleft");
const modifyContainer = document.querySelector("#modifySection");

for(let i = 0; i< chooseDanie.length; i++) {
    chooseDanie[i].addEventListener("click", function (event) {
        const id = this.nextElementSibling.querySelector("input[type=hidden]").value;
            const data = {modify: id};
            console.log(data);

            fetch("selectDanie", {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data)
            }).then(function (response) {
                return response.json();
            }).then(function (skladniki) {
                console.log(skladniki);
                modifyContainer.innerHTML = "";
                loadSkladniki(skladniki)
            });
        });
}

function loadSkladniki(skladniki) {
    skladniki.forEach(skladnik => {
        console.log(skladnik);
        createSkladnik(skladnik);
    });
}

function createSkladnik(skladnik) {
    const template = document.querySelector("#modify-template");

    const clone = template.content.cloneNode(true);

    const name = clone.querySelector(".jedzenieName");
    name.innerText = skladnik.nazwa;

    const numberInput = clone.querySelector("input[type=number]");
    numberInput.name = "number"+skladnik.id;

    const jednostka = clone.querySelector(".jednostka");
    jednostka.innerText = skladnik.jednostka;

    const idInput = clone.querySelector("input[type=hidden]");
    idInput.value = skladnik.id;

    modifyContainer.appendChild(clone);

    document.querySelector('button[name=edit]').disabled = false;
}
