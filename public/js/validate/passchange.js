const form1 = document.querySelector('form[action="changePhone"]');
const form2 = document.querySelector('form[action="changePass"]');
const confirmedPasswordInput = form2.querySelector('input[name="newpassConfirmed"]');
const newPassInput = confirmedPasswordInput.previousElementSibling;
const phoneInput = form1.querySelector('input[name="phone"]');


function isPasswordStrong(pass) {
    if (pass.length > 7)
        return true;
    return false;
}

function isGoodLength(str, min, max){
    if (str.length < min || str.length > max)
        return false;
    return true;
}

function arePasswordsSame(password, confirmedPassword) {
    return password === confirmedPassword;
}

function markValidation(element, condition) {
    !condition ? element.classList.add('no-valid') : element.classList.remove('no-valid');
}

function validatePassword1() {
    setTimeout(function () {
            markValidation(newPassInput, isPasswordStrong(newPassInput.value));
        },
        1000
    );
}


function validatePassword2() {
    setTimeout(function () {
            const condition = arePasswordsSame(
                newPassInput.value,
                confirmedPasswordInput.value
            );
            markValidation(confirmedPasswordInput, condition);
        },
        1000
    );
}

function validatePhone(){
    setTimeout(function () {
            const condition = isGoodLength(
                phoneInput.value,
                0,
                15
            );
            markValidation(phoneInput, condition);
        },
        1000
    );
}
newPassInput.addEventListener('keyup', validatePassword1);
confirmedPasswordInput.addEventListener('keyup', validatePassword2);
phoneInput.addEventListener('keyup', validatePhone);