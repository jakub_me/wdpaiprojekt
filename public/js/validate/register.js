const form = document.querySelector("form");
const emailInput = form.querySelector('input[name="email"]');
const confirmedPasswordInput = form.querySelector('input[name="confirmPassword"]');
const passwordInput = confirmedPasswordInput.previousElementSibling;
const firstnameInput = form.querySelector('input[name="firstname"]');
const surnameInput = form.querySelector('input[name="surname"]');
const phoneInput = form.querySelector('input[name="phone"]');

function isEmail(email) {
    return /\S+@\S+\.\S+/.test(email);
}
function isPasswordStrong(pass) {
    if (pass.length > 7)
        return true;
    return false;
}

function isGoodLength(str, min, max){
    if (str.length < min || str.length > max)
        return false;
    return true;
}

function arePasswordsSame(password, confirmedPassword) {
    return password === confirmedPassword;
}

function markValidation(element, condition) {
    !condition ? element.classList.add('no-valid') : element.classList.remove('no-valid');
}

function validateEmail() {
    setTimeout(function () {
            markValidation(emailInput, isEmail(emailInput.value));
        },
        1000
    );
}

function validatePassword1() {
    setTimeout(function () {
            markValidation(passwordInput, isPasswordStrong(passwordInput.value));
        },
        1000
    );
}

function validatePassword2() {
    setTimeout(function () {
            const condition = arePasswordsSame(
                passwordInput.value,
                confirmedPasswordInput.value
            );
            markValidation(confirmedPasswordInput, condition);
        },
        1000
    );
}

function validateFirstname(){
    setTimeout(function () {
            const condition = isGoodLength(
                firstnameInput.value,
                1,
                50
            );
            markValidation(firstnameInput, condition);
        },
        1000
    );
}

function validateSurname(){
    setTimeout(function () {
            const condition = isGoodLength(
                surnameInput.value,
                1,
                50
            );
            markValidation(surnameInput, condition);
        },
        1000
    );
}

function validatePhone(){
    setTimeout(function () {
            const condition = isGoodLength(
                phoneInput.value,
                0,
                15
            );
            markValidation(phoneInput, condition);
        },
        1000
    );
}
emailInput.addEventListener('keyup', validateEmail);
passwordInput.addEventListener('keyup', validatePassword1);
confirmedPasswordInput.addEventListener('keyup', validatePassword2);
firstnameInput.addEventListener('keyup', validateFirstname);
surnameInput.addEventListener('keyup', validateSurname);
phoneInput.addEventListener('keyup', validatePhone);