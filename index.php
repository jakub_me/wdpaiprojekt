<?php


function print_r2($val)
{
    echo '<pre>';
    print_r($val);
    echo '</pre>';
}


require "Routing.php";

$path = trim($_SERVER['REQUEST_URI'], '/');
$path = parse_url($path, PHP_URL_PATH);


Routing::get('', 'HomepageController');  //main1, mainmenu
Routing::get('menu', 'MenuController');
Routing::get('login', 'SecurityController');
Routing::get('register', 'SecurityController');
Routing::get('passchange', 'SecurityController');
Routing::get('panel', 'PanelController');
Routing::get('koszyk', 'KoszykController');
Routing::get('dbpanel', 'DBController');
Routing::get('baseusers', 'DBUsersController');
Routing::get('basejednostki', 'DBJednostkaController');
Routing::get('basejedzenie', 'DBJedzenieController');
Routing::get('basemenu', 'DBMenuController');

//login
Routing::post('login', 'SecurityController');

//register
Routing::post('newUser', 'SecurityController');

//passchange
Routing::post('changePhone', 'SecurityController');
Routing::post('changePass', 'SecurityController');

//basejednostki
Routing::post('modifyJednostka', 'DBJednostkaController');
Routing::post('addJednostka', 'DBJednostkaController');

//basejedzenie

Routing::post('modifyJedzenie', 'DBJedzenieController');
Routing::post('addJedzenie', 'DBJedzenieController');

//basemenu
Routing::post('manageMenu', 'DBMenuController');
Routing::post('deleteDanie', 'DBMenuController');
Routing::post('selectDanie', 'DBMenuController');

//baseusers
Routing::post('modifyUser', 'DBUsersController');

        //TODO Routing::post('searchUsersByRights', 'DBUsersController');


//-------------functions--------

//logging out
Routing::get('logout','HomepageController');
Routing::post('logout','HomepageController');

//-----------------------------------------


// TODO koszyk
Routing::post('koszykBuy', 'KoszykController');


Routing::run($path);